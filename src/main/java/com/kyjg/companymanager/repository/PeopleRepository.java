package com.kyjg.companymanager.repository;

import com.kyjg.companymanager.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeopleRepository extends JpaRepository<People, Long> {
}
