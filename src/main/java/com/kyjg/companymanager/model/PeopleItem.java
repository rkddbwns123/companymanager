package com.kyjg.companymanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PeopleItem {
    private String name;
    private String phone;
}
