package com.kyjg.companymanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PeopleRequest {
    private String name;
    private String phone;
    private LocalDate birthday;
}
