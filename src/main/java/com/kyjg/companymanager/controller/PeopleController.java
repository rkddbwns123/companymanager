package com.kyjg.companymanager.controller;

import com.kyjg.companymanager.model.PeopleItem;
import com.kyjg.companymanager.model.PeopleRequest;
import com.kyjg.companymanager.service.PeopleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/people")
public class PeopleController {
    private final PeopleService peopleService;
    @PostMapping("/data")
    public String setPeople(@RequestBody PeopleRequest request) {
        peopleService.setPeople(request.getName(), request.getPhone(), request.getBirthday());
        return "OK";
    }

    @GetMapping("/peoples")
    public List<PeopleItem> getPeoples() {
        List<PeopleItem> result = peopleService.getPeoples();
        return result;
    }
}
